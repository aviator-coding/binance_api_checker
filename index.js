/*  Author SupaaHiro 20181227 v1 - Binance API Check Tool
    
    Tested with node v11.6 on Windows 10 Version 1809 (Build 17763.195)
    
    Configuration (Optional)
      If you wanna test a SIGNED HMAC-SHA256 POST /api/v3/order/test request
      1. Replace <my_api_code> with your api code
      2. Replace <my_secret_key> with your API secret
    
    How to run:
      node index.js 
 */

const https = require('https');
const querystring = require('querystring');
const crypto = require("crypto");
const BINANCE_API_BASEURL = 'api.binance.com';
const BINANCE_API_KEY = '<my_api_code>'
const BINANCE_API_SECRET = '<my_secret_key>'
const CONSOLE_COLOR_RED = '\x1b[31m', CONSOLE_COLOR_GREEN = '\x1b[32m', CONSOLE_COLOR_YELLOW = '\x1b[33m', CONSOLE_COLOR_DEFAULT = '\x1b[0m';
const BINANCE_MAX_TIMESTAMP_DELTA = 5000; /* See https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md, Check Timing security */

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

/* GET /api/v1/ping
     Test connectivity to the Rest API.
 */
var apiPing = function(result) {
  https.get('https://' + BINANCE_API_BASEURL + '/api/v1/ping', (res) => {
    res.on('data', (chunk) => {
	    result(true);
    });
  }).on('error', (e) => {
    console.error(e);
	result(false);
  });  
};

/* GET /api/v1/time
     Test connectivity to the Rest API and get the current server time.
 */
var apiServerTime = function(result) {
  https.get('https://' + BINANCE_API_BASEURL + '/api/v1/time', (res) => {
    // console.log('statusCode:', res.statusCode);
    // console.log('headers:', res.headers);
    
    res.on('data', (chunk) => {
      // process.stdout.write(chunk);
      var raw = chunk.toString('utf8');
      var json = JSON.parse(raw);
      
      result(Number(json.serverTime));
    });
  
  }).on('error', (e) => {
    console.error(e);
  }); 
} 

/* POST /api/v3/order/test
     Test new order creation and signature/recvWindow long. Creates and validates a new order but does not send it into the matching engine.
 */
var apiTestOrder = function(result) {
    if (BINANCE_API_KEY == '' || BINANCE_API_KEY == '<my_api_code>' || BINANCE_API_SECRET == '' || BINANCE_API_SECRET == '<my_secret_key>')
    {
      result('', null);
      return;
    }
    
    var query = {
      'symbol':    'BNBBTC',
      'side':      'SELL',
      'type':      'MARKET',
      'quantity':  '100',
      'timestamp': Date.now().toString()
    }
    query.signature = crypto.createHmac("sha256", BINANCE_API_SECRET).update(querystring.stringify(query)).digest("hex");
	var post_data = querystring.stringify(query);
	
	var post_options = {
      host: BINANCE_API_BASEURL,
      port: 443,
      path: '/api/v3/order/test',
      method: 'POST',
	    headers: {
		    'X-MBX-APIKEY':   BINANCE_API_KEY,
            'Content-Type':   'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(post_data)
        }
      };
    
	  // console.log(post_options);
	  // console.log(post_data);
	  
    // Set up the request
    var post_req = https.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
		      // process.stdout.write(chunk);
          var raw = chunk.toString('utf8');
          var json = JSON.parse(raw);
          
		      if (isEmptyObject(json))
            result(json, true);
	        else
			      result(json, false);
        });
    });
    
    // Post the data
    post_req.write(post_data);
    post_req.end();
};

function printSuccess() {
  console.log('\nEverything seems OK !');
  
}

// CHECK API (WITH A TEST ORDER)
var checkApi = function() {
  process.stdout.write("Check API: ");
  apiTestOrder(
    (json, result) => {
	  var success = result;
      if (success == null)
      {
        console.log(CONSOLE_COLOR_YELLOW + 'IGNORED' + CONSOLE_COLOR_DEFAULT);
        console.log('\tNo secret key! Skipping APITEST - It require a HMAC-SHA256 signature');
        success = true;
      }
      else if (success != true)
      {
        console.log(CONSOLE_COLOR_RED + 'ERROR' + CONSOLE_COLOR_DEFAULT);
        console.log('Reply = %s', JSON.stringify(json));
      }
	    else
        console.log(CONSOLE_COLOR_GREEN + 'OK' + CONSOLE_COLOR_DEFAULT);

	  // Next Test -> END
	  if (success) { printSuccess(); }
  });
};

// CHECK TIME
var checkTime = function() {
	process.stdout.write("Check Time: ");
	apiServerTime((result) => {
	  var serverTime = result;
	  var localTime = Date.now();
      var delta = serverTime - localTime;
	  var success = (Math.abs(delta) < BINANCE_MAX_TIMESTAMP_DELTA) ? true: false;
	  
      if (!success)
        console.log(CONSOLE_COLOR_RED + 'ERROR' + CONSOLE_COLOR_DEFAULT);
	  else {
		console.log(CONSOLE_COLOR_GREEN + 'OK' + CONSOLE_COLOR_DEFAULT);
	  }
	  
	  // Log server time
	  console.log('\tServer time %d', serverTime);
      console.log('\tLocal time %d',  localTime);
	  console.log('\tdelta %d mS',    delta);
	  
	  // Next Test -> CHECK API
	  if (success) { checkApi(); }
	});
};

// CHECK PING
var checkPing = function() {
  process.stdout.write("Check Ping: ");
  apiPing(
    (result) => {
      if (result != true)
      {
        console.log(CONSOLE_COLOR_RED + 'ERROR' + CONSOLE_COLOR_DEFAULT);
        return;
      }
      console.log(CONSOLE_COLOR_GREEN + 'OK' + CONSOLE_COLOR_DEFAULT);
	  
	  // Next Test -> CHECK TIME
	  checkTime();
  });
};

console.log('Test connection with BINANCE');
console.log('\tServer\t' + BINANCE_API_BASEURL);
if (BINANCE_API_KEY == '<my_api_code>')
  console.log('\tApi\tN/A');
else
  console.log('\tApi\t' + BINANCE_API_KEY);
checkPing();



