# binance_api_checker

This is a tool I made for test my Binance APIs.  

Functions
> GET  /api/v1/ping       -> Check connectivity to the API server  
  GET  /api/v1/time       -> Check local/remote time sync  
  POST /api/v3/order/test -> Check HMAC-SHA256 signed request  

Configuration (Optional) - If you wanna test a SIGNED HMAC-SHA256 POST /api/v3/order/test request  
> - Replace <my_api_code> with your api code  
  - Replace <my_secret_key> with your API secret  

How to run:  
> node index.js  

Tested with node v11.6 on Windows 10 Version 1809 (Build 17763.195)  

Author SupaaHiro 20181227 v1 - Binance API Check Tool  
  